# Chapter 1: What is MongoDB?

1. Why is MongoDB a NoSQL database?
   - [x] Because it uses a structured way to store and access data
   - [x] Because it does **not** utilize tables, rows and columns to organize data

2. Select the statements that together help build the most complete definition of the MongoDB database:
   - [x] The MongoDB database is an organized way to store and access data.
   - [ ] MongoDB database organizes documents in rows and columns.
   - [ ] MongoDB's database uses tables of related data.
   - [x] MongoDB is a NoSQL database that uses documents to store data in an organized way.

3. In MongoDB how does a document relate to a collection?
   - [ ] Collections are tables of documents and other collections.
   - [x] Collections consist of one or many documents.
   - [ ] Collections are documents that are organized in rows and columns.
   - [ ] Documents are made up of collections.

4. In a MongoDB Document what is the role of fields and values?
   - [x] A field is a unique identifier for a specific datapoint.
   - [ ] Values do not have to be attached to fields, and can be stand alone data points.
   - [x] Each field has a value associated with it.

5. How is MongoDB Atlas related to MongoDB the Database?
   - [x] They both are MongoDB products.
   - [ ] MongoDB Database has the same functionality as Atlas, but without the friendly user interface.
   - [ ] Atlas is a MongoDB service that can work with any database, but in this course it will be used with MongoDB.
   - [x] Atlas has many tools and services within it that are built specifically for the MongoDB Database.

6. Did the logical size of the dataset and the number of operations increase in your cluster view similar to how it did in this image?
   - [x] yes
   - [ ] no


# Chapter 2: Importing, Exporting, and Querying Data

1. Which of the following documents is correct JSON?
   - [x] {"name" : "Devi", "age": 19, "major": "Computer Science"}
   - [ ] {name : "Devi", age: 19, major: "Computer Science"}
   - [ ] ["name" : "Devi", "age": 19, "major": "Computer Science"]

2. Write BSON or JSON in the numbered blanks in the following sentences to make them true:

   MongoDB stores data in `bson`, and you can then view it in `json`. `bson` is faster to parse and lighter to store than `json`. `json` supports fewer data types than `bson`.
   
3. Which of the following commands will add a collection that is stored in animals.json to an Atlas cluster?
   - [x] mongoimport
   - [ ] mongodump
   - [ ] mongoexport
   - [ ] mongorestore

4. In the sample_training.trips collection a person with birth year 1961 took a trip that started at "Howard St & Centre St". What was the end station name for that trip?

   `"South End Ave & Liberty St"`

5. What does `it` do in the mongo shell?
   - [ ] Warns about imminent termination
   - [ ] `it` is not a mongo shell command
   - [ ] Interferes with the task
   - [x] Iterates through the cursor results

6. Which of the following statements are true about the mongo shell?
   - [x] It is a fully functioning JavaScript interpreter
   - [ ] mongo shell automatically returns an ordered list of results
   - [x] It allows you to interact with your MongoDB instance without using a Graphical User Interface

# Chapter 3: Creating and Manipulating Documents

1. How does the value of _id get assigned to a document?
   - [ ] _id field values are sequential integer values.
   - [x] It is automatically generated as an ObjectId type value.
   - [ ] When a document is inserted a random field is picked to serve as the _id field.
   - [x] You can select a non ObjectId type value when inserting a new document, as long as that value is unique to this document.

2. Select all true statements from the following list:
   - [ ] MongoDB can always store duplicate documents in the same collection regardless of the _id value.
   - [x] MongoDB can store duplicate documents in the same collection, as long as their _id values are different.
   - [x] If a document is inserted without a provided _id value, then the _id field and value will be automatically generated for the inserted document before insertion.
   - [ ] There is no way to ensure that duplicate records are not stored in MongoDB.
   - [ ] If a document is inserted without a provided _id value, then that document will fail to be inserted and cause a write error.

3. Which of the following commands will successfully insert 3 new documents into an empty pets collection?
   - [x] db.pets.insert([{ "_id": 1, "pet": "cat" },\
          { "_id": 1, "pet": "dog" },\
          { "_id": 3, "pet": "fish" },\
          { "_id": 4, "pet": "snake" }], { "ordered": false })
   - [ ] db.pets.insert([{ "_id": 1, "pet": "cat" },\
          { "_id": 1, "pet": "dog" },\
          { "_id": 3, "pet": "fish" },\
          { "_id": 4, "pet": "snake" }], { "ordered": true })
   - [x] db.pets.insert([{ "pet": "cat" },\
          { "pet": "dog" },\
          { "pet": "fish" }])
   - [x] db.pets.insert([{ "_id": 1, "pet": "cat" },\
          { "_id": 2, "pet": "dog" },\
          { "_id": 3, "pet": "fish" },\
          { "_id": 3, "pet": "snake" }])

4. MongoDB has a flexible data model, which means that you can have fields that contain documents, or arrays as their values. Select any invalid MongoDB documents from the given choices:
    - [ ] { "_id": 1, "pet": "cat", "attributes": [ { "coat": "fur", "type": "soft" }, { "defense": "claws", "location": "paws", "nickname": "murder mittens" } ], "name": "Furball" }
    - [ ] { "_id": 1, "pet": "cat", "fur": "soft", "claws": "sharp", "name": "Furball" }
    - [ ] { "_id": 1, "pet": "cat", "attributes": { "coat": "soft fur", "paws": "cute but deadly" }, "name": "Furball" }
    - [x] None of the above.

5. Given a pets collection where each document has the following structure and fields:
    > ```
    > {
    > "_id": ObjectId("5ec414e5e722bb1f65a25451"),
    > "pet": "wolf",
    > "domestic?": false,
    > "diet": "carnivorous",
    > "climate": ["polar", "equatorial", "continental", "mountain"]
    > }
    > ```
    Which of the following commands will add new fields to the updated documents?
   - [ ] `db.pets.updateMany({ "pet": "cat" }, {"$set": { "domestic?": true, "diet": "mice" }})`
   - [ ] `db.pets.updateMany({ "pet": "cat" }, { "$set": { "climate": "continental" }})`
   - [x] `db.pets.updateMany({ "pet": "cat" }, { "$push": { "climate": "continental", "look": "adorable" } })`
   - [x] `db.pets.updateMany({ "pet": "cat" }, { "$set": { "type": "dangerous", "look": "adorable" }})`

6. Does removing all collections in a database also remove the database?
   - [x] yes
   - [ ] no

7. Which of the following commands will delete a collection named villains?
   - [ ] db.villains.dropAll()
   - [x] db.villains.drop()
   - [ ] db.villains.delete()

# Chapter 4: Advanced CRUD Operations

1. How many documents in the sample_training.zips collection have fewer than 1000 people listed in the pop field?

   `8065`

2. What is the difference between the number of people born in 1998 and the number of people born after 1998 in the sample_training.trips collection?

   `6`

3. Using the sample_training.routes collection find out which of the following statements will return all routes that have at least one stop in them?
   - [x] `db.routes.find({ "stops": { "$gt": 0 }}).pretty()`
   - [ ] `db.routes.find({ "stops": { "$gte": 0 }}).pretty()`
   - [ ] `db.routes.find({ "stops": { "$lt": 0 }}).pretty()`
   - [x] `db.routes.find({ "stops": { "$ne": 0 }}).pretty()`

4. How many businesses in the sample_training.inspections dataset have the inspection result "Out of Business" and belong to the "Home Improvement Contractor - 100" sector?

   `4`

5. Which is the most succinct query to return all documents from the sample_training.inspections collection where the inspection date is either "Feb 20 2015", or "Feb 21 2015" and the company is not part of the "Cigarette Retail Dealer - 127" sector?
   - [ ] `db.inspections.find({ "$and": [{ "$or": [{ "date": "Feb 20 2015" }, { "date": "Feb 21 2015" } ] }, {"sector": { "$ne":"Cigarette Retail Dealer - 127" }}]}).pretty()`
   - [x] `db.inspections.find({ "$or": [ { "date": "Feb 20 2015" }, { "date": "Feb 21 2015" } ], "sector": { "$ne": "Cigarette Retail Dealer - 127" }}).pretty()`
   - [ ] `db.inspections.find({ "$or": [ { "date": "Feb 20 2015" }, { "date": "Feb 21 2015" }], "$not": { "sector": "Cigarette Retail Dealer - 127" }}).pretty()`
  
6. How many zips in the sample_training.zips dataset are neither over-populated nor under-populated?
   
   `11193`

7. How many companies in the sample_training.companies dataset were either founded in 2004 **and** either have the social category_code **or** web category_code, **or** were founded in the month of October **and** also either have the social category_code **or** web category_code?

   `149`

8. What are some of the uses for the $ sign in MQL?
   - [ ] $ makes the world go round.
   - [x] $ denotes an operator.
   - [ ] $ changes the data type of the given value to a monetary denomination.
   - [x] $ signifies that you are looking at the value of that field rather than the field name.

9. Which of the following statements will find all the companies that have more employees than the year in which they were founded?
   - [ ] `db.companies.find({ "number_of_employees": { "$gt":  "$founded_year" }}).count()`
   - [ ] `db.companies.find({ "$expr": { "$gt": [ "$founded_year", "$number_of_employees" ]}}).count()`
   - [x] `db.companies.find({ "$expr": { "$lt": [ "$founded_year", "$number_of_employees" ]}}).count()`
   - [x] `db.companies.find({ "$expr": { "$gt": [ "$number_of_employees", "$founded_year" ]}}).count()`

10. How many companies in the sample_training.companies collection have the same permalink as their twitter_username?

    `1299`

11. What is the name of the listing in the sample_airbnb.listingsAndReviews dataset that accommodates more than 6 people and has exactly 50 reviews?

    `Sunset Beach Lodge Retreat`

12. Using the sample_airbnb.listingsAndReviews collection find out how many documents have the "property_type" "House", and include "Changing table" as one of the "amenities"?

    `11`

13. Which of the following queries will return all listings that have "Free parking on premises", "Air conditioning", and "Wifi" as part of their amenities, and have at least 2 bedrooms in the sample_airbnb.listingsAndReviews collection?
    - [ ] `db.listingsAndReviews.find({ "amenities": [ "Free parking on premises", "Wifi", "Air conditioning" ]}, "bedrooms": { "$gte": 2 } } ).pretty()`
    - [x] `db.listingsAndReviews.find({ "amenities": { "$all": [ "Free parking on premises", "Wifi", "Air conditioning" ] }, "bedrooms": { "$gte":  2 } } ).pretty()`
    - [ ] `db.listingsAndReviews.find({ "amenities": { "$all": [ "Free parking on premises", "Wifi", "Air conditioning" ] }, "bedrooms": { "$lte": 2 } }).pretty()`
    - [ ] `db.listingsAndReviews.find({ "amenities": "Free parking on premises", "amenities": "Wifi", "amenities": "Air conditioning", "bedrooms": { "$gte": 2 }}).pretty()`

14. How many companies in the sample_training.companies collection have offices in the city of Seattle?

    `117`

15. Which of the following queries will return only the names of companies from the sample_training.companies collection that had exactly 8 funding rounds?
    - [ ] `db.companies.find({ "funding_rounds": { "$size": 8 } }, { "name": 1 })`
    - [ ] `db.companies.find({ "funding_rounds": { "$size": 8 } }, { "name": 0, "_id": 1 })`
    - [x] `db.companies.find({ "funding_rounds": { "$size": 8 } }, { "name": 1, "_id": 0 })`

16. How many trips in the sample_training.trips collection started at stations that are to the west of the -74 longitude coordinate?

    `1928`

17. How many inspections from the sample_training.inspections collection were conducted in the city of NEW YORK?

    `18279`

18. Which of the following queries will return the names and addresses of all listings from the sample_airbnb.listingsAndReviews collection where the first amenity in the list is "Internet"?
    - [ ] `db.listingsAndReviews.find({ "amenities.$0": "Internet" }, { "name": 1, "address": 1 }).pretty()`
    - [x] `db.listingsAndReviews.find({ "amenities.0": "Internet" }, { "name": 1, "address": 1 }).pretty()`
    - [ ] `db.listingsAndReviews.find({ "amenities.[0]": "Internet" }, { "name": 1, "address": 1 }).pretty()`

# Chapter 5: Indexing and Aggregation Pipeline

1. What room types are present in the sample_airbnb.listingsAndReviews collection?
   - [ ] Kitchen
   - [x] Private room
   - [ ] Living Room
   - [ ] Apartment
   - [x] Shared room
   - [x] Entire home/apt
   - [ ] House

2. What are the differences between using aggregate() and find()?
   - [x] aggregate() can do what find() can and more.
   - [ ] find() can do what the aggregate() can do and more.
   - [ ] find() allows us to compute and reshape data in the cursor.
   - [x] aggregate() allows us to compute and reshape data in the cursor.

3. Which of the following commands will return the name and founding year for the 5 oldest companies in the sample_training.companies collection?
   - [ ] db.companies.find({ "name": 1, "founded_year": 1 }).sort({ "founded_year": 1 }).limit(5)
   - [x] db.companies.find({ "founded_year": { "$ne": null }}, { "name": 1, "founded_year": 1 }).sort({ "founded_year": 1 }).limit(5)
   - [x] db.companies.find({ "founded_year": { "$ne": null }}, { "name": 1, "founded_year": 1 }).limit(5).sort({ "founded_year": 1 })
   - [ ] db.companies.find({ },{ "name": 1, "founded_year": 1 }).sort({ "founded_year": 1 }).limit(5)

4. In what year was the youngest bike rider from the sample_training.trips collection born?
   - [x] 1999
   - [ ] 1998
   - [ ] 1885
   - [ ] 2000

5. Jameela often queries the sample_training.routes collection by the src_airport field like this:
`db.routes.find({ "src_airport": "MUC" }).pretty()` Which command will create an index that will support this query?
   - [ ] db.routes.addIndex({ "src_airport": 1 })
   - [ ] db.routes.generateIndex({ "src_airport": -1 })
   - [ ] db.routes.makeIndex({ "src_airport": 1 })
   - [x] db.routes.createIndex({ "src_airport": -1 })

6. What is data modeling?
   - [ ] a way to build your application based on how your data is stored
   - [ ] a way to show off your amazing data to everyone using graphs and illustrations
   - [ ] a way to decide whether to store your data in the cloud or locally
   - [x] a way to organize fields in a document to support your application performance and querying capabilities

7. How does the upsert option work?
   - [ ] It is used with the update operator, and needs to have its value specified every time that the update operator is called.
   - [x] By default upsert is set to false.
   - [x] When upsert is set to true and the query predicate returns an empty cursor, the update operation creates a new document using the directive from the query predicate and the update predicate.
   - [x] When upsert is set to false and the query predicate returns an empty cursor then there will be no updated documents as a result of this operation.

# Chapter 6: Next Steps

1. What actions are available to you via the Aggregation Builder in the Atlas Data Explorer?
   - [x] Syntax for each selected aggregation stage.
   - [ ] An indication of which indexes are used by the pipeline.
   - [x] Export pipeline to a programming language.
   - [x] A preview of the data in the pipeline at each selected stage.

2. What is MongoDB Charts?
   - [x] A product that helps you build visualizations of the data stored in your Atlas Cluster.
   - [ ] An app that allows you to embed visualizations that are created in other applications into your website.
   - [ ] A feature that displays performance data about your Atlas Cluster.

2. What is MongoDB Compass?
   - [ ] A type of map-based chart that is available with MongoDB Charts
   - [x] MongoDB's Graphical User Interface Product
   - [ ] A term used to denote an object that stores geospatial coordinates in the collection.
